@extends('layouts.master')

@section('head.title')
    Tieu de cua bai viet
@stop
@section('body.content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <a href="/" class="btn btn-link">
                    <i class="fa fa-arrow-left"></i> Back to home
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2>{{ $article->title }}</h2>
                <p>{{ $article->content }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <a href="{{ route('article.edit',$article->id) }}" class="btn btn-info">Cập nhật</a>
                <form action="{{ route('article.destroy',$article->id) }}" method="post" style="display: inline">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Xóa</button>
                </form>
            </div>
        </div>
    </div>
@stop
