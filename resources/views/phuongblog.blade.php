@extends('layouts.frontend')

@push('styles')
    <link rel="stylesheet" href="{{ asset_custom('css/wow.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-sm-6 col-sm-offset-4">
            <img src="https://www.onblastblog.com/wp-content/uploads/2017/08/blogger-logo-624x357.jpg" alt=""
                 width="400" height="200" class="wow rubberBand">
        </div>
    </div>
    <div class="row">
        <nav class="navbar navbar-default" role="navigation" style="background: lightblue">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Phuong Blogger</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">HOME</a></li>
                    <li><a href="#">ABOUT</a></li>
                    <li><a href="#">INTRODUCTION</a></li>
                    <li><a href="#">LIFT STYLE</a></li>
                    <li><a href="#">FAMILY</a></li>
                </ul>
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default" style="background-color: #69F0AE">Submit</button>
                </form>
            </div><!-- /.navbar-collapse -->
        </nav>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="/img/choben.jpg" alt="Los Angeles" style="width:100%; height: 600px">
                    </div>

                    <div class="item">
                        <img src="/img/choben2.jpg" alt="Chicago" style="width:100%; height: 600px">
                    </div>

                    <div class="item">
                        <img src="/img/tamdao.jpg" alt="New york" style="width:100%; height: 600px;">
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <i style="margin-top: 300px; color: #bce8f1" class="fa fa-arrow-circle-left"></i>
                    {{--<span class="sr-only">Previous</span>--}}
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <i style="margin-top: 300px; color: #bce8f1" class="fa fa-arrow-circle-right"></i>
                    {{--<span class="sr-only">Next</span>--}}
                </a>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 50px">
        <div class="col-sm-8">
            <img src="/img/halong.jpg" class="wow fadeInUp" alt="" style="width: 100%">
            <h2 class="text-center wow wobble" style="color: #4cae4c">Hạ Long & Hotel Mường Thanh</h2>
            <p style="font-size: 18px">Những kỷ niệm thật tuyệt vời của chuyến đi Hạ Long cùng công ty Lock&Lock. Những
                kỷ niệm thật tuyệt vời của chuyến đi Hạ Long cùng công ty Lock&Lock. </p>
            <h4 class="text-center" style="color: #f66d9b">Hạ Long / 25-7-2018</h4>
            <br>
            <img src="/img/anhem.jpg" class="wow fadeInUp" alt="" style="width: 100%">
        </div>
        <div class="col-sm-4">
            <h3 class="text-center wow jello" style="font-weight: bold;color: #26C6DA">Bức ảnh nổi bật</h3>
            <i class="fa fa-minus col-sm-7 col-sm-offset-5" style="font-size: 25px"></i>
            <b>Bức ảnh đẹp nhất làm việc tại Lock&Lock</b>
            <p>Những kỷ niệm thật tuyệt vời với bức ảnh do tự tay tôi chụp</p>
            <img src="/img/lock.jpg" class="wow fadeInUp" alt="" style="width: 100%">
            <h3 class="text-center wow jello" style="margin-top: 60px; font-weight: bold;color: #26C6DA">Lock&Lock</h3>
            <i class="fa fa-minus col-sm-7 col-sm-offset-5"
               style="margin-top: 15px; margin-bottom:20px; font-size: 25px"></i>
            <img src="/img/lock2.jpg" class="wow fadeInUp" alt="" style="width: 100%">
            <h3 class="text-center wow jello"
                style="margin-top: 30px;margin-bottom:20px;font-weight: bold;color: #26C6DA">Theo dõi tôi</h3>
            <i class="fa fa-minus col-sm-7 col-sm-offset-5" style="font-size: 25px;margin-bottom: 10px"></i>
            <div class="row">
                <div class="col-sm-2 col-lg-offset-1">
                    <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook-official wow slideInLeft" style="font-size: 35px;color: #0D47A1"></i></a>
                </div>
                <div class="col-sm-2">
                    <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter-square wow jackInTheBox" style="font-size: 35px;color: aqua"></i></a>
                </div>
                <div class="col-sm-2">
                    <a href="#" title="Skype" target="_blank"><i class="fa fa-skype wow rollIn" style="font-size: 35px;color: #2196F3"></i></a>
                </div>
                <div class="col-sm-2">
                    <a href="#" title="Instagram" target="_blank"><i class="fa fa-instagram wow slideInDown" style="font-size: 35px;color: #F57C00"></i></a>
                </div>
                <div class="col-sm-2">
                    <a href="#" title="Dribble" target="_blank"><i class="fa fa-dribbble wow slideInUp" style="font-size: 35px;color: orangered"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 40px">
        <hr>
        <div class="col-sm-7 col-sm-offset-5" style="margin-bottom: 20px">
            <p>Copyright &copy; by <b style="color: #607D8B">Phương Blogger</b></p>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset_custom('js/wow.min.js') }}"></script>
    <script>
        new WOW().init();
    </script>
@endpush
