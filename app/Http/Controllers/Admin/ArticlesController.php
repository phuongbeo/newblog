<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;
use Illuminate\Support\Facades\Input;

class ArticlesController extends Controller
{
    public function index()
    {
        $articles = Article::paginate(3);
        return view('articles.index', compact('articles'));
    }

    public function show($id)
    {
        $article = Article::find($id);
        return view('articles.show', compact('article'));
    }

    public function create()
    {
        return view('articles.create');
    }

    public function store(Request $request)
    {
        $inputs = $request->all();
        $request->validate([
            'title' => 'required|min:10',
            'content' => 'required|min:20'
        ]);

        $inputs['status'] = true;
        Article::create($inputs);
//        $title = Input::get('title');
//        $content = Input::get('content');

//        Article::create([
//           'title' => $title,
//           'content' => $content
//        ]);
        return redirect()->route('article.index');
    }

    public function edit($id)
    {
        $article = Article::find($id);
//        dd($article->title);
        return view('articles.edit', compact('article'));
    }

    public function update($id, Request $request)
    {
        $inputs = $request->all();
        $request->validate([
            'title' => 'required|min:10',
            'content' => 'required|min:20'
        ]);
        $article = Article::find($id);
        $article->fill($inputs);
        $article->save();
        return redirect()->route('article.index');

    }
    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();
        return redirect()->route('article.index');

    }
}
