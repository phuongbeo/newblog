<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class LoginController extends Controller
{
    public function getLogin()
    {
        return view('login');
    }
    public function postLogin(Request $request)
    {
        $inputs = $request->all();
        $request->validate([
           'email' => 'required|email',
            'password' => 'required|min8'
        ]);
        $inputs['status'] = true;
        User::create($inputs);

    }
}
